## Evolution of a docker container for an easy to setup and use CoCo development environment.
## To build:
##     docker build -t leep/coco-dev .
## To start up for use:
##     docker run --name coco-dev -it -d --volume D:\PathTo\ProjectFolder:/projects/build-root leep/coco-dev
## Note that the path for --volume before the : is case sensitive
## 
## To use to compile in Visual Studio Code in Terminal window:
##   docker exec --workdir /projects/build-root coco-dev make

FROM ubuntu:20.04

WORKDIR /root

RUN apt update
RUN apt upgrade -y
RUN apt install -y g++ make git python3 inotify-tools 
RUN alias python=python3
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata
RUN apt-get install -y software-properties-common

RUN add-apt-repository ppa:tormodvolden/m6809
RUN echo deb http://ppa.launchpad.net/tormodvolden/m6809/ubuntu bionic main >> /etc/apt/sources.list.d/tormodvolden-m6809.list
RUN echo deb http://ppa.launchpad.net/tormodvolden/m6809/ubuntu trusty main >> /etc/apt/sources.list.d/tormodvolden-m6809.list
RUN echo deb http://ppa.launchpad.net/tormodvolden/m6809/ubuntu precise main >> /etc/apt/sources.list.d/tormodvolden-m6809.list
RUN apt update
RUN apt upgrade -y
RUN apt install -y --force-yes cmoc=0.1.85-0~tormod
RUN apt install -y --force-yes lwtools=4.22-0~tormod~~trusty
RUN apt install -y --force-yes toolshed=2.2-0~tormod

WORKDIR /projects
