# docker-build
* You shouldn't need to build the image from the included `Docker` file since the image has already been build is available on the Docker Repository as `leep/coco-dev`.
* If you do need/want to build the `coco-dev` image locally, `docker build -t {yourname}/coco-dev` and edit the `coco-dev-run.ps1` script to reference `{yourname}/coco-dev` instead of `leep/coco-dev`.
## Scripts included
Note: The Docker Desktop GUI must be started before running these scripts.  Once you start the GUI, you can close the GUI and Docker will continue to be available.
* `coco-dev-run.ps1` - Create the `coco-dev` container from the `leep/coco-dev` image.  The image will be pulled from the docker repository if needed.
    - Only needs to ran once.  Once you run it, the container `coco-dev` will exist, even after restarting your computer.
    - Mounts the host folder `C:\CoCo\Projects\` to the `/projects/` folder in the container.  Edit `coco-dev-run.ps1` to change the host folder if needed.
* `coco-dev-start.ps1` - Starts the container if it's stopped, and starts an interactive bash shell.
    - The bash shell starts in the `/projects` folder.  You can `cd` into the project folder you want.  Assuming you have a `makefile`, you can run `make` to build.
    - Enter `exit` to exit the interactive bash shell.

## My Workflow
### Start up process
* Start Docker Destop.  Once it opens, you can close it.  Opening it starts Docker running.  There may be a way to make just Docker (not the Docker Desktop GUI) auto start on Windows start up.
* Start Drivewire 4 and leave it running.
    - Previously mounted `.dsk` images will still be mounted.
    - If you haven't built the project yet, you may need to build it once to generate the `.dsk` image, then come back to Drivewire to mount it.
* Turn on the CoCo.
    * Enter `DRIVE 2,#?` where `?` is the number of the Driverwire "drive" you mounted the `.dsk` image in.
    * Enter `DRIVE 2` to switch to drive 2.
    * You should now be able to do `DIR` to see the contents of the `.dsk` image.
    * If you haven't build the project before, you will have do this after you build and mount the `.dsk` image in Drivewire.
* Start Visual Studio Code with the `C:\CoCo\Projects\tileScroll\` folder open.
* In VS Code's terminal
    - Run `C:\CoCo\docker-build\coco-dev-start`.  This leaves the terminal at a bash prompt in the `/projects` folder.
    - Run `cd tileScroll` to enter the same folder VS Code has open.

### Workflow
* Make code changes in VS Code.
* Hit [Ctrl]+[~] to swtich to VS Code's terminal.
* Enter `make` to build the project.
    - The build process creates a `.dsk` image in the `.\build\` folder.
    - The `.dsk` image stays mounted in Drivewire, so changes are immediately available on the CoCo.
        - If this is your first time building, mount the newly created `.dsk` image in Drivewire and in the CoCo.
* On the CoCo, RUN/LOAD/LOADM your project and test it.
* Repeat ad-infinitum.
